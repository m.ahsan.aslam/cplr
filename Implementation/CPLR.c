#include <stdio.h> 
#include <stdlib.h> 
#include <unistd.h> 
#include <string.h>
#include "CPLR.h"

#define max_error 0
#define ROW 2
#define COL 10


extern float x[ROW][COL], y[ROW][COL], z[ROW][COL], segx[ROW][COL], segy[ROW][COL], segz[ROW][COL];
extern float t_n1,tx_n1,x_n1,Bx_n1,ty_n1,y_n1,By_n1,tz_n1,z_n1,Bz_n1;
extern int start,n;

CPLR(int k)
{
 int anchor=0;
 float t_n,tx_n,x_n,Bx_n,x_axis;
 float     ty_n,y_n,By_n,y_axis;
 float     tz_n,z_n,Bz_n,z_axis;
 float SSRx=0;
 float SSRy=0;
 float SSRz=0;
 float SSR=0;
 while(anchor==0 && start <= 10)
	{
	n = n+1;
	x_axis = x[1][start]-segx[1][k-1];		y_axis = y[1][start]-segy[1][k-1];		z_axis = z[1][start]-segz[1][k-1];
	t_n = t_n1+(((n*n)-t_n1)/n);
	tx_n = tx_n1+(((n*x_axis)-tx_n1)/n);		ty_n = ty_n1+(((n*y_axis)-ty_n1)/n);		tz_n = tz_n1+(((n*z_axis)-tz_n1)/n);
	x_n = x_n1+(((x_axis*x_axis)-x_n1)/n);		y_n = y_n1+(((y_axis*y_axis)-y_n1)/n);		z_n = z_n1+(((z_axis*z_axis)-z_n1)/n);
	Bx_n = (tx_n/t_n);				By_n = (ty_n/t_n);				Bz_n = (tz_n/t_n);
	SSRx = (x_n-(Bx_n*tx_n))*n;			SSRy = (y_n-(By_n*ty_n))*n;			SSRz = (z_n-(Bz_n*tz_n))*n;
	SSR = SSRx+SSRy+SSRz;
	start = start+1;
	if(SSR <= max_error)
		{
		t_n1 = t_n;
		tx_n1 = tx_n;		ty_n1 = ty_n;		tz_n1 = tz_n;
		x_n1 = x_n;		y_n1 = y_n;		z_n1 = z_n;
		Bx_n1 = Bx_n;		By_n1 = By_n;		Bz_n1 = Bz_n;
		}
	else	{
		anchor=1;
		}
	
	}
 
 segx[0][k] = segx[0][k-1]+n-1;			segy[0][k] = segy[0][k-1]+n-1;			segz[0][k] = segz[0][k-1]+n-1;
 segx[1][k] = segx[1][k-1]+(Bx_n1*(n-1));	segy[1][k] = segy[1][k-1]+(By_n1*(n-1));	segz[1][k] = segz[1][k-1]+(Bz_n1*(n-1));
 x_axis = x[1][start-1]-segx[1][k-1];		y_axis = y[1][start-1]-segy[1][k-1];		z_axis = z[1][start-1]-segz[1][k-1];
 n = 1;
 t_n1 = 1;
 tx_n1 = x_axis;				ty_n1 = y_axis;					tz_n1 = z_axis;
 x_n1 = x_axis*x_axis;				y_n1 = y_axis*y_axis;				z_n1 = z_axis*z_axis;
 Bx_n1 = x_axis;				By_n1 = y_axis;					Bz_n1 = z_axis;
 return 0;
}
