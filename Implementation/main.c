#include <stdio.h> 
#include <stdlib.h> 
#include <unistd.h> 
#include <string.h>
#include "CPLR.h" 

#define max_error 0
#define ROW 2
#define COL 10


float x[ROW][COL] ={
	{0,1,2,3,4,5,6,7,8,9},	
	{22,30,12,5,10,13,25,27,3,8}   
};


float y[ROW][COL] ={
	{0,1,2,3,4,5,6,7,8,9},	
	{8,17,20,2,45,41,5,33,7}   
};


float z[ROW][COL] ={
	{0,1,2,3,4,5,6,7,8,9},	
	{42,39,6,21,28,37,23,29,8,10}   
};



int start,n;
float t_n1,tx_n1,x_n1,Bx_n1;
float      ty_n1,y_n1,By_n1;
float      tz_n1,z_n1,Bz_n1;
float segx[ROW][COL]={
	{0,0,0,0,0,0,0,0,0,0},	
	{0,0,0,0,0,0,0,0,0,0}   
};

float segy[ROW][COL]={
	{0,0,0,0,0,0,0,0,0,0},	
	{0,0,0,0,0,0,0,0,0,0}   
};

float segz[ROW][COL]={
	{0,0,0,0,0,0,0,0,0,0},	
	{0,0,0,0,0,0,0,0,0,0}   
};






int main()
{
 int k,end;
 start = 1;
 n=0;
 t_n1=0;
 tx_n1=0;	ty_n1=0;	tz_n1=0;
 x_n1=0;	y_n1=0;		z_n1=0;
 Bx_n1=0;	By_n1=0;	Bz_n1=0;
 k=0;

 segx[0][0]=x[0][0];
 segy[0][0]=y[0][0];
 segz[0][0]=z[0][0];
 segx[1][0]=x[1][0];
 segy[1][0]=y[1][0];
 segz[1][0]=z[1][0];

 while(start < 11 )
	{
	k += 1;	
	CPLR(k);
	}
 printf("\n x_axis \n");
 for(int j=0;j<2;j++)
	{
	printf("\n");	
	for(int l=0;l<10;l++)
	printf("%f	",segx[j][l]);
	}
 printf("\n y_axis \n");
 for(int j=0;j<2;j++)
	{
	printf("\n");	
	for(int l=0;l<10;l++)
	printf("%f	",segy[j][l]);
	}
 printf("\n z_axis \n");
 for(int j=0;j<2;j++)
	{
	printf("\n");	
	for(int l=0;l<10;l++)
	printf("%f	",segz[j][l]);
	}
 return 0;
}





